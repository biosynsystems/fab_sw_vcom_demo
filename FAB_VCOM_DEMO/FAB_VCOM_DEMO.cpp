// FAB_VCOM_DEMO.cpp : Defines the entry point for the console application.
//


/*
Use Precompiled Headers
*/
#include "stdafx.h"

/*
Library Definitions
*/
#include <stdio.h>
#include <sys/stat.h>
#include <time.h>
#include <iostream>
#include <string>

/*
System Specific Definitions
*/
#include <io.h>
#include <windows.h>
#include <winbase.h>
#include <mmsystem.h>
#include "FTD2XX.H"

/*
Various application definitions
*/
#include "FAB_VCOM_DEMO.h"
static HANDLE rtfab_handle = INVALID_HANDLE_VALUE;
static FT_W32_ReadFile_t RTFAB_ReadFile = ReadFile;
static FT_W32_WriteFile_t RTFAB_WriteFile = (FT_W32_WriteFile_t)WriteFile;
static void CALLBACK data_timer_callback(UINT wTimerID, UINT msg, DWORD dwUser, DWORD dw1, DWORD dw2);
static void unload_FTDI_DLL(HMODULE DLL_handle);

int main()
{
	MMRESULT data_timer;
	HMODULE DLL_handle = NULL;
	FT_W32_CreateFile_t RTFAB_CreateFile = CreateFile;
	FT_W32_GetCommState_t RTFAB_GetCommState = (FT_W32_GetCommState_t)GetCommState;
	FT_W32_SetCommState_t RTFAB_SetCommState = (FT_W32_SetCommState_t)SetCommState;
	FT_W32_SetCommTimeouts_t RTFAB_SetCommTimeouts = (FT_W32_SetCommTimeouts_t)SetCommTimeouts;
	FT_W32_PurgeComm_t RTFAB_PurgeComm = PurgeComm;
	FT_W32_CloseHandle_t RTFAB_CloseHandle = CloseHandle;
	FT_ListDevices_t FT_ListDevices = NULL;
	//TODO: check use of this not used in 13 sensor ver
	static FT_GetQueueStatus_t FT_GetQueueStatus = NULL;

	FTDCB rtfab_config;
	FTTIMEOUTS rtfab_timeouts;
	unsigned char header_bytes[FULL_HEADER_LENGTH];
	unsigned char rx_buffer[256];
	DWORD dwNum;
	DWORD dwDevNum;
	char rt_devname[FTDI_NAME_LENGTH + 1];

	char cbuf;

	TIMECAPS tc;
	DWORD timer_resolution;

	std::string pathname;
	std::cin >> pathname;

	pathname += sizeof(RTFAB_PREFIX) - 1;   // Step forward past the colon.
	if (strncmp(pathname.c_str(), "COM", 3) == 0)   // Pre-validate it.
	{
		// DLL_handle is already initialised to NULL.

		// Create the UNC pathname of the port:
		strcpy(rt_devname, RT_DEV_PREFIX);
		strncat(rt_devname, pathname.c_str(), RT_DEV_PORT_LENGTH);

		// Open the port:
		rtfab_handle = RTFAB_CreateFile(rt_devname, GENERIC_READ | GENERIC_WRITE,
			0, NULL, OPEN_EXISTING, 0, NULL);
	}
	else if (strncmp(pathname.c_str(), "FT", 2) == 0)
	{
		dwDevNum = atoi(&pathname[2]);
		if (dwDevNum < 1 || dwDevNum > 20)
		{
			return FRD_NO_SUCH_FILE;
		}

		// Attempt to load the DLL:
		// EXPERIMENTAL IF() FOR SYSTEM MERGE!!!!!!!!!!!!!!!!
		if (DLL_handle == NULL) {
			DLL_handle = LoadLibrary(FTDI_DLL);
		}
		if (DLL_handle == NULL)
		{
			return FRD_NO_DLL;
		}

		RTFAB_CreateFile = (FT_W32_CreateFile_t)GetProcAddress(DLL_handle, "FT_W32_CreateFile");
		RTFAB_GetCommState = (FT_W32_GetCommState_t)GetProcAddress(DLL_handle, "FT_W32_GetCommState");
		RTFAB_SetCommState = (FT_W32_SetCommState_t)GetProcAddress(DLL_handle, "FT_W32_SetCommState");
		RTFAB_SetCommTimeouts = (FT_W32_SetCommTimeouts_t)GetProcAddress(DLL_handle, "FT_W32_SetCommTimeouts");
		RTFAB_PurgeComm = (FT_W32_PurgeComm_t)GetProcAddress(DLL_handle, "FT_W32_PurgeComm");
		RTFAB_ReadFile = (FT_W32_ReadFile_t)GetProcAddress(DLL_handle, "FT_W32_ReadFile");
		RTFAB_WriteFile = (FT_W32_WriteFile_t)GetProcAddress(DLL_handle, "FT_W32_WriteFile");
		RTFAB_CloseHandle = (FT_W32_CloseHandle_t)GetProcAddress(DLL_handle, "FT_W32_CloseHandle");
		FT_ListDevices = (FT_ListDevices_t)GetProcAddress(DLL_handle, "FT_ListDevices");
		FT_GetQueueStatus = (FT_GetQueueStatus_t)GetProcAddress(DLL_handle, "FT_GetQueueStatus");
		if (RTFAB_CreateFile == NULL ||
			RTFAB_GetCommState == NULL ||
			RTFAB_SetCommState == NULL ||
			RTFAB_SetCommTimeouts == NULL ||
			RTFAB_PurgeComm == NULL ||
			RTFAB_ReadFile == NULL ||
			RTFAB_WriteFile == NULL ||
			RTFAB_CloseHandle == NULL ||
			FT_ListDevices == NULL ||
			FT_GetQueueStatus == NULL)
		{
			unload_FTDI_DLL(DLL_handle);
			return FRD_DLL_ERROR;
		}

		// Find the FTDI devices:
		if (FT_ListDevices(&dwNum, NULL, FT_LIST_NUMBER_ONLY) != FT_OK ||
			dwNum == 0 || dwDevNum > dwNum)
		{
			unload_FTDI_DLL(DLL_handle);
			return FRD_NO_SUCH_FILE;
		}

		// Get the USB location number in dwNum.
		if (FT_ListDevices((PVOID)(dwDevNum - 1), &dwNum,
			FT_LIST_BY_INDEX | FT_OPEN_BY_LOCATION) != FT_OK)
		{
			unload_FTDI_DLL(DLL_handle);
			return FRD_NO_SUCH_FILE;
		}

		// The FTDI D2XX Programmer's Guide (2006) does not document
		// FT_W32_CreateFile() as being able to open devices by
		// location, but it works!!
		// dwNum is the USB location number.
		rtfab_handle = RTFAB_CreateFile((LPCSTR)dwNum, GENERIC_READ | GENERIC_WRITE,
			0, NULL, OPEN_EXISTING, FT_OPEN_BY_LOCATION, NULL);
	}
	else
	{
		return FRD_NO_SUCH_FILE;
	}

	if (rtfab_handle == INVALID_HANDLE_VALUE)
	{
		unload_FTDI_DLL(DLL_handle);
		return FRD_CANNOT_OPEN;
	}

	// Get some initial values for the port configuration:
	RTFAB_GetCommState(rtfab_handle, &rtfab_config);


	// Modify them to suit us:
	rtfab_config.BaudRate = CBR_115200;
	rtfab_config.fBinary = TRUE;
	rtfab_config.fParity = FALSE;
	rtfab_config.fOutxCtsFlow = FALSE;
	rtfab_config.fOutxDsrFlow = FALSE;
	rtfab_config.fDtrControl = DTR_CONTROL_ENABLE;
	rtfab_config.fDsrSensitivity = FALSE;
	rtfab_config.fOutX = FALSE;
	rtfab_config.fInX = FALSE;
	rtfab_config.fNull = FALSE;
	rtfab_config.fRtsControl = RTS_CONTROL_ENABLE;
	rtfab_config.fAbortOnError = FALSE;
	rtfab_config.ByteSize = 8;
	rtfab_config.Parity = NOPARITY;
	rtfab_config.StopBits = ONESTOPBIT;

	// Configure the port with the new parameters:
	RTFAB_SetCommState(rtfab_handle, &rtfab_config);

	// Set the time-out behaviour of the port for reading the header:
	rtfab_timeouts.ReadIntervalTimeout = 0;         // These three values for the read timeouts
	rtfab_timeouts.ReadTotalTimeoutMultiplier = 0;  // specifies for ReadFile() wait for one
	rtfab_timeouts.ReadTotalTimeoutConstant = 1000; // second for the amount of data requested.
	rtfab_timeouts.WriteTotalTimeoutMultiplier = 0;
	rtfab_timeouts.WriteTotalTimeoutConstant = 0;
	RTFAB_SetCommTimeouts(rtfab_handle, &rtfab_timeouts);


	// Clear any junk from the transmit queue (shouldn't be necessary, but
	// done for good measure):
	RTFAB_PurgeComm(rtfab_handle, PURGE_TXABORT | PURGE_TXCLEAR);

	// Keep asking the realtime bridge for a header:
	while (TRUE)
	{
		// Reset it to the idle state first.
		cbuf = RTFAB_RESET_CMD;
		RTFAB_WriteFile(rtfab_handle, &cbuf, 1, &dwNum, NULL);
		Sleep(100); // Wait 100 milliseconds for reset.

		RTFAB_PurgeComm(rtfab_handle, PURGE_RXABORT | PURGE_RXCLEAR);

		// Ask for header.
		cbuf = RTFAB_SEND_HEADER_CMD;
		RTFAB_WriteFile(rtfab_handle, &cbuf, 1, &dwNum, NULL);


		// This ReadFile call will wait for data per the
		// ReadTotalTimeoutConstant set above.
		if (RTFAB_ReadFile(rtfab_handle, header_bytes, FULL_HEADER_LENGTH, &dwNum, NULL) &&
			dwNum == FULL_HEADER_LENGTH &&
			header_bytes[0] == FRD_FORMAT_ID)
		{
			// Got it!
			std::cout << header_bytes << std::endl;
			break;
		}
		// Otherwise, loop around and try again.
	}

	// Set the time-out behaviour of the port for reading data:
	rtfab_timeouts.ReadIntervalTimeout = MAXDWORD;  // These three values for the read timeouts
	rtfab_timeouts.ReadTotalTimeoutMultiplier = 0;  // specifies for ReadFile() to return
	rtfab_timeouts.ReadTotalTimeoutConstant = 0;    // immediately with any data available.
	rtfab_timeouts.WriteTotalTimeoutMultiplier = 0;
	rtfab_timeouts.WriteTotalTimeoutConstant = 0;
	RTFAB_SetCommTimeouts(rtfab_handle, &rtfab_timeouts);


	// Send initial send-data command:
	cbuf = RTFAB_SEND_DATA_CMD;
	RTFAB_WriteFile(rtfab_handle, &cbuf, 1, &dwNum, NULL);

	// Start a timer to keep polling the realtime bridge for data
	timeGetDevCaps(&tc, sizeof(TIMECAPS));
	timer_resolution = min(max(tc.wPeriodMin, 100), tc.wPeriodMax); // 100 ms good enough.
	timeBeginPeriod(timer_resolution);
	data_timer = timeSetEvent(1000, timer_resolution, data_timer_callback,
		0, TIME_PERIODIC);

	while (TRUE)
	{
		Sleep(100); 
		//Should be a data stream at this point, grab as much as possible to fill rx_buffer then print
		if (RTFAB_ReadFile(rtfab_handle, rx_buffer, 256, &dwNum, NULL))
		{
			// Got it!
			std::cout << header_bytes << std::endl;
		}
	}

	return 0;
}

static void unload_FTDI_DLL(HMODULE DLL_handle)
{
	if (DLL_handle != NULL)
	{
		FreeLibrary(DLL_handle);
		DLL_handle = NULL;
	}
}

static void CALLBACK data_timer_callback(UINT wTimerID, UINT msg, DWORD dwUser, DWORD dw1, DWORD dw2)
{
	char cbuf;
	DWORD dwNum;

	// Send another command to keep the realtime bridge going:
	cbuf = RTFAB_SEND_DATA_CMD;
	RTFAB_WriteFile(rtfab_handle, &cbuf, 1, &dwNum, NULL);
}
