/*
Abstract:

Custom FTDI USB device driver for FAB systems.
Modified version of FTDI FTD2XX.h file from FTDI.

Revision History:

Feb-18-2007  William Hue    Changed everything for dynamic loading of DLL.

*/

#ifndef FAB_VCOM_DEMO_H
#define FAB_VCOM_DEMO_H
#define RTFAB_PREFIX    "RTFAB:"
#define RT_DEV_PREFIX           "\\\\.\\"
#define RT_DEV_PREFIX_LENGTH    (sizeof(RT_DEV_PREFIX) - 1)
#define RT_DEV_PORT_LENGTH      5   // COMxx
#define RT_DEV_NAME_LENGTH      (RT_DEV_PREFIX_LENGTH + RT_DEV_PORT_LENGTH)
#define FTDI_NAME_LENGTH        64
#define RT_CALIBRATION_SECONDS  20

#define MAXHOSTNAMELEN  256
#define FTDI_DLL "FTD2XX.DLL"

/*
Packet Definitions
*/
#define FRD_FORMAT_ID   0x35
#define DATETIME_SIZE   7 
#define FRDFAB_HEADER_SIZE  (16 + DATETIME_SIZE)
#define FULL_HEADER_LENGTH (1 + FRDFAB_HEADER_SIZE)

/*
Command Definitions
*/
#define RTFAB_RESET_CMD         'R' // Sent by FRD2FAB to reset Realtime Bridge.
#define RTFAB_SEND_HEADER_CMD   'H' // Sent by FRD2FAB until header received.
#define RTFAB_SEND_DATA_CMD     'D' // Sent once per second by FRD2FAB.

/*
Error Definitions
*/
#define FRD_SUCCESS         0
#define FRD_ALREADY_OPEN    (-10)
#define FRD_IS_TTY          (-11)
#define FRD_NO_SUCH_FILE    (-12)
#define FRD_NO_READ_ACCESS  (-13)
#define FRD_CANNOT_OPEN     (-14)
#define FRD_BAD_FORMAT      (-15)
#define FRD_BAD_VERSION     (-16)
#define FRD_FILE_CORRUPTED  (-17)
#define FRD_NO_DLL          (-100)
#define FRD_DLL_ERROR       (-101)

#endif