/*++

Module Name:

    ftd2xx.h

Abstract:

    Custom FTDI USB device driver for FAB systems.
	Modified version of FTDI FTD2XX.h file from FTDI.

Environment:

    kernel & user mode

Revision History:

    Feb-18-2007  William Hue    Changed everything for dynamic loading of DLL.
	
--*/


#ifndef FTD2XX_H
#define FTD2XX_H

#ifdef FTD2XX_EXPORTS
#define FTD2XX_API __declspec(dllexport)
#else
#define FTD2XX_API __declspec(dllimport)
#endif


typedef PVOID	FT_HANDLE;
typedef ULONG	FT_STATUS;

//
// Device status
//
enum {
    FT_OK,
    FT_INVALID_HANDLE,
    FT_DEVICE_NOT_FOUND,
    FT_DEVICE_NOT_OPENED,
    FT_IO_ERROR,
    FT_INSUFFICIENT_RESOURCES,
    FT_INVALID_PARAMETER,
    FT_INVALID_BAUD_RATE,

    FT_DEVICE_NOT_OPENED_FOR_ERASE,
    FT_DEVICE_NOT_OPENED_FOR_WRITE,
    FT_FAILED_TO_WRITE_DEVICE,
    FT_EEPROM_READ_FAILED,
    FT_EEPROM_WRITE_FAILED,
    FT_EEPROM_ERASE_FAILED,
	FT_EEPROM_NOT_PRESENT,
	FT_EEPROM_NOT_PROGRAMMED,
	FT_INVALID_ARGS,
	FT_NOT_SUPPORTED,
	FT_OTHER_ERROR
};


#define FT_SUCCESS(status) ((status) == FT_OK)

//
// FT_OpenEx Flags
//

#define FT_OPEN_BY_SERIAL_NUMBER    1
#define FT_OPEN_BY_DESCRIPTION      2
#define FT_OPEN_BY_LOCATION			4

//
// FT_ListDevices Flags (used in conjunction with FT_OpenEx Flags
//

#define FT_LIST_NUMBER_ONLY			0x80000000
#define FT_LIST_BY_INDEX			0x40000000
#define FT_LIST_ALL					0x20000000

#define FT_LIST_MASK (FT_LIST_NUMBER_ONLY|FT_LIST_BY_INDEX|FT_LIST_ALL)

//
// Baud Rates
//

#define FT_BAUD_300			300
#define FT_BAUD_600			600
#define FT_BAUD_1200		1200
#define FT_BAUD_2400		2400
#define FT_BAUD_4800		4800
#define FT_BAUD_9600		9600
#define FT_BAUD_14400		14400
#define FT_BAUD_19200		19200
#define FT_BAUD_38400		38400
#define FT_BAUD_57600		57600
#define FT_BAUD_115200		115200
#define FT_BAUD_230400		230400
#define FT_BAUD_460800		460800
#define FT_BAUD_921600		921600

//
// Word Lengths
//

#define FT_BITS_8			(UCHAR) 8
#define FT_BITS_7			(UCHAR) 7
#define FT_BITS_6			(UCHAR) 6
#define FT_BITS_5			(UCHAR) 5

//
// Stop Bits
//

#define FT_STOP_BITS_1		(UCHAR) 0
#define FT_STOP_BITS_1_5	(UCHAR) 1
#define FT_STOP_BITS_2		(UCHAR) 2

//
// Parity
//

#define FT_PARITY_NONE		(UCHAR) 0
#define FT_PARITY_ODD		(UCHAR) 1
#define FT_PARITY_EVEN		(UCHAR) 2
#define FT_PARITY_MARK		(UCHAR) 3
#define FT_PARITY_SPACE		(UCHAR) 4

//
// Flow Control
//

#define FT_FLOW_NONE        0x0000
#define FT_FLOW_RTS_CTS     0x0100
#define FT_FLOW_DTR_DSR     0x0200
#define FT_FLOW_XON_XOFF    0x0400

//
// Purge rx and tx buffers
//
#define FT_PURGE_RX         1
#define FT_PURGE_TX         2

//
// Events
//

typedef void (*PFT_EVENT_HANDLER)(DWORD,DWORD);

#define FT_EVENT_RXCHAR		    1
#define FT_EVENT_MODEM_STATUS   2

//
// Timeouts
//

#define FT_DEFAULT_RX_TIMEOUT   300
#define FT_DEFAULT_TX_TIMEOUT   300

//
// Device types
//

typedef ULONG	FT_DEVICE;

enum {
    FT_DEVICE_BM,
    FT_DEVICE_AM,
    FT_DEVICE_100AX,
    FT_DEVICE_UNKNOWN,
    FT_DEVICE_2232C,
    FT_DEVICE_232R
};

#ifdef __cplusplus
extern "C" {
#endif

typedef FT_STATUS (WINAPI *FT_ListDevices_t)(
	PVOID pArg1,
	PVOID pArg2,
	DWORD Flags
	);

typedef FT_STATUS (WINAPI *FT_ListDevices_t)(
	PVOID pArg1,
	PVOID pArg2,
	DWORD Flags
	);

typedef FT_STATUS (WINAPI *FT_GetQueueStatus_t)(
    FT_HANDLE ftHandle,
	DWORD *dwRxBytes
	);


//
// structure to hold program data for FT_Program function
//
typedef struct ft_program_data {

	DWORD Signature1;			// Header - must be 0x00000000 
	DWORD Signature2;			// Header - must be 0xffffffff
	DWORD Version;				// Header - FT_PROGRAM_DATA version
								//          0 = original
	                            //          1 = FT2232C extensions
								//			2 = FT232R extensions

	WORD VendorId;				// 0x0403
	WORD ProductId;				// 0x6001
	char *Manufacturer;			// "FTDI"
	char *ManufacturerId;		// "FT"
	char *Description;			// "USB HS Serial Converter"
	char *SerialNumber;			// "FT000001" if fixed, or NULL
	WORD MaxPower;				// 0 < MaxPower <= 500
	WORD PnP;					// 0 = disabled, 1 = enabled
	WORD SelfPowered;			// 0 = bus powered, 1 = self powered
	WORD RemoteWakeup;			// 0 = not capable, 1 = capable
	//
	// Rev4 extensions
	//
	UCHAR Rev4;					// non-zero if Rev4 chip, zero otherwise
	UCHAR IsoIn;				// non-zero if in endpoint is isochronous
	UCHAR IsoOut;				// non-zero if out endpoint is isochronous
	UCHAR PullDownEnable;		// non-zero if pull down enabled
	UCHAR SerNumEnable;			// non-zero if serial number to be used
	UCHAR USBVersionEnable;		// non-zero if chip uses USBVersion
	WORD USBVersion;			// BCD (0x0200 => USB2)
	//
	// FT2232C extensions
	//
	UCHAR Rev5;					// non-zero if Rev5 chip, zero otherwise
	UCHAR IsoInA;				// non-zero if in endpoint is isochronous
	UCHAR IsoInB;				// non-zero if in endpoint is isochronous
	UCHAR IsoOutA;				// non-zero if out endpoint is isochronous
	UCHAR IsoOutB;				// non-zero if out endpoint is isochronous
	UCHAR PullDownEnable5;		// non-zero if pull down enabled
	UCHAR SerNumEnable5;		// non-zero if serial number to be used
	UCHAR USBVersionEnable5;	// non-zero if chip uses USBVersion
	WORD USBVersion5;			// BCD (0x0200 => USB2)
	UCHAR AIsHighCurrent;		// non-zero if interface is high current
	UCHAR BIsHighCurrent;		// non-zero if interface is high current
	UCHAR IFAIsFifo;			// non-zero if interface is 245 FIFO
	UCHAR IFAIsFifoTar;			// non-zero if interface is 245 FIFO CPU target
	UCHAR IFAIsFastSer;			// non-zero if interface is Fast serial
	UCHAR AIsVCP;				// non-zero if interface is to use VCP drivers
	UCHAR IFBIsFifo;			// non-zero if interface is 245 FIFO
	UCHAR IFBIsFifoTar;			// non-zero if interface is 245 FIFO CPU target
	UCHAR IFBIsFastSer;			// non-zero if interface is Fast serial
	UCHAR BIsVCP;				// non-zero if interface is to use VCP drivers
	//
	// FT232R extensions
	//
	UCHAR UseExtOsc;			// Use External Oscillator
	UCHAR HighDriveIOs;			// High Drive I/Os
	UCHAR EndpointSize;			// Endpoint size

	UCHAR PullDownEnableR;		// non-zero if pull down enabled
	UCHAR SerNumEnableR;		// non-zero if serial number to be used

	UCHAR InvertTXD;			// non-zero if invert TXD
	UCHAR InvertRXD;			// non-zero if invert RXD
	UCHAR InvertRTS;			// non-zero if invert RTS
	UCHAR InvertCTS;			// non-zero if invert CTS
	UCHAR InvertDTR;			// non-zero if invert DTR
	UCHAR InvertDSR;			// non-zero if invert DSR
	UCHAR InvertDCD;			// non-zero if invert DCD
	UCHAR InvertRI;				// non-zero if invert RI

	UCHAR Cbus0;				// Cbus Mux control
	UCHAR Cbus1;				// Cbus Mux control
	UCHAR Cbus2;				// Cbus Mux control
	UCHAR Cbus3;				// Cbus Mux control
	UCHAR Cbus4;				// Cbus Mux control

	UCHAR RIsVCP;				// non-zero if using VCP drivers

} FT_PROGRAM_DATA, *PFT_PROGRAM_DATA;

//Note: Cannot compile with unicode support
typedef FT_HANDLE (WINAPI *FT_W32_CreateFile_t)(
	LPCSTR					lpszName,
	DWORD					dwAccess,
	DWORD					dwShareMode,
	LPSECURITY_ATTRIBUTES	lpSecurityAttributes,
	DWORD					dwCreate,
	DWORD					dwAttrsAndFlags,
	HANDLE					hTemplate
	);

typedef BOOL (WINAPI *FT_W32_CloseHandle_t)(
    FT_HANDLE ftHandle
	);

typedef BOOL (WINAPI *FT_W32_ReadFile_t)(
    FT_HANDLE ftHandle,
    LPVOID lpBuffer,
    DWORD nBufferSize,
    LPDWORD lpBytesReturned,
	LPOVERLAPPED lpOverlapped
    );

typedef BOOL (WINAPI *FT_W32_WriteFile_t)(
    FT_HANDLE ftHandle,
    LPVOID lpBuffer,
    DWORD nBufferSize,
    LPDWORD lpBytesWritten,
	LPOVERLAPPED lpOverlapped
    );

//
// Win32 COMM API type functions
//
typedef struct _FTCOMSTAT {
    DWORD fCtsHold : 1;
    DWORD fDsrHold : 1;
    DWORD fRlsdHold : 1;
    DWORD fXoffHold : 1;
    DWORD fXoffSent : 1;
    DWORD fEof : 1;
    DWORD fTxim : 1;
    DWORD fReserved : 25;
    DWORD cbInQue;
    DWORD cbOutQue;
} FTCOMSTAT, *LPFTCOMSTAT;

typedef struct _FTDCB {
    DWORD DCBlength;      /* sizeof(FTDCB)                   */
    DWORD BaudRate;       /* Baudrate at which running       */
    DWORD fBinary: 1;     /* Binary Mode (skip EOF check)    */
    DWORD fParity: 1;     /* Enable parity checking          */
    DWORD fOutxCtsFlow:1; /* CTS handshaking on output       */
    DWORD fOutxDsrFlow:1; /* DSR handshaking on output       */
    DWORD fDtrControl:2;  /* DTR Flow control                */
    DWORD fDsrSensitivity:1; /* DSR Sensitivity              */
    DWORD fTXContinueOnXoff: 1; /* Continue TX when Xoff sent */
    DWORD fOutX: 1;       /* Enable output X-ON/X-OFF        */
    DWORD fInX: 1;        /* Enable input X-ON/X-OFF         */
    DWORD fErrorChar: 1;  /* Enable Err Replacement          */
    DWORD fNull: 1;       /* Enable Null stripping           */
    DWORD fRtsControl:2;  /* Rts Flow control                */
    DWORD fAbortOnError:1; /* Abort all reads and writes on Error */
    DWORD fDummy2:17;     /* Reserved                        */
    WORD wReserved;       /* Not currently used              */
    WORD XonLim;          /* Transmit X-ON threshold         */
    WORD XoffLim;         /* Transmit X-OFF threshold        */
    BYTE ByteSize;        /* Number of bits/byte, 4-8        */
    BYTE Parity;          /* 0-4=None,Odd,Even,Mark,Space    */
    BYTE StopBits;        /* 0,1,2 = 1, 1.5, 2               */
    char XonChar;         /* Tx and Rx X-ON character        */
    char XoffChar;        /* Tx and Rx X-OFF character       */
    char ErrorChar;       /* Error replacement char          */
    char EofChar;         /* End of Input character          */
    char EvtChar;         /* Received Event character        */
    WORD wReserved1;      /* Fill for now.                   */
} FTDCB, *LPFTDCB;

typedef struct _FTTIMEOUTS {
    DWORD ReadIntervalTimeout;          /* Maximum time between read chars. */
    DWORD ReadTotalTimeoutMultiplier;   /* Multiplier of characters.        */
    DWORD ReadTotalTimeoutConstant;     /* Constant in milliseconds.        */
    DWORD WriteTotalTimeoutMultiplier;  /* Multiplier of characters.        */
    DWORD WriteTotalTimeoutConstant;    /* Constant in milliseconds.        */
} FTTIMEOUTS,*LPFTTIMEOUTS;

typedef BOOL (WINAPI *FT_W32_GetCommState_t)(
    FT_HANDLE ftHandle,
    LPFTDCB lpftDcb
	);

typedef BOOL (WINAPI *FT_W32_PurgeComm_t)(
    FT_HANDLE ftHandle,
	DWORD dwMask
	);

typedef BOOL (WINAPI *FT_W32_SetCommState_t)(
    FT_HANDLE ftHandle,
    LPFTDCB lpftDcb
	);

typedef BOOL (WINAPI *FT_W32_SetCommTimeouts_t)(
    FT_HANDLE ftHandle,
    FTTIMEOUTS *pTimeouts
	);

//
// Device information
//

typedef struct _ft_device_list_info_node {
	ULONG Flags;
    ULONG Type;
	ULONG ID;
	DWORD LocId;
	char SerialNumber[16];
	char Description[64];
	FT_HANDLE ftHandle;
} FT_DEVICE_LIST_INFO_NODE;

#ifdef __cplusplus
}
#endif


#endif  /* FTD2XX_H */

